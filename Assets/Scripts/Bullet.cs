﻿using SpaceShip;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
    [SerializeField] private float damage = 1f;
    [SerializeField] private float speed = 10f;
    private BaseSpaceShip _ownerShip;

    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.gravityScale = 0f;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check if damage own ship
        if (other.TryGetComponent<BaseSpaceShip>(out var baseSpaceShip))
            if (baseSpaceShip == _ownerShip)
                return;

        if (!other.TryGetComponent<IDamagable>(out var target)) return;
        gameObject.SetActive(false);
        target.TakeDamage(damage);
    }

    public void Init(Vector2 dir, BaseSpaceShip owner)
    {
        _rigidbody.velocity = dir * speed;
        _ownerShip = owner;
    }
}