﻿using PlayerShip;
using UnityEngine;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float enemyShipSpeed = 5;

        private Transform _playerTransform;
        private readonly float chasingThresholdDistance = 1.0f;

        private void Awake()
        {
            _playerTransform = FindObjectOfType<PlayerController>().transform;
        }

        private void Update()
        {
            MoveToPlayer();
        }

        private void MoveToPlayer()
        {
            var enemyPosition = transform.position;
            var playerPosition = _playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position

            var
                vectorToTarget = enemyPosition - playerPosition; // vector from this object towards the target location
            var directionToTarget = vectorToTarget.normalized;
            var velocity = directionToTarget * enemyShipSpeed;

            var distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance) transform.Translate(velocity * Time.deltaTime);
        }
    }
}