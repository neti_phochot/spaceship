﻿using UnityEngine;

public class BulletPool : MonoBehaviour, IPool<Bullet>
{
    [SerializeField] private Bullet bulletPrefab;
    [SerializeField] private int initBullet = 5;
    private Bullet[] _bullets;

    private int _currentPoolIndex;
    public static BulletPool GetInstance { get; private set; }

    private void Awake()
    {
        if (GetInstance != null && GetInstance != this)
        {
            Destroy(gameObject);
            return;
        }

        GetInstance = this;
    }

    private void Start()
    {
        Prewarm(initBullet);
    }

    public void Prewarm(int init)
    {
        _bullets = new Bullet[init];
        for (var i = 0; i < init; i++)
        {
            var bullet = Instantiate(bulletPrefab);
            bullet.gameObject.SetActive(false);
            _bullets[i] = bullet;
        }
    }

    public Bullet Request()
    {
        var bullet = _bullets[_currentPoolIndex++ % _bullets.Length];
        bullet.gameObject.SetActive(true);
        return bullet;
    }

    public void Return(Bullet member)
    {
        member.gameObject.SetActive(false);
    }
}