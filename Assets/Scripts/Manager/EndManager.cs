﻿using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class EndManager : MonoBehaviour, IVisible
    {
        [SerializeField] private Text scoreText;
        [SerializeField] private Button restartButton;

        private GameManager _gameManager;

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText can't be null");
            Debug.Assert(restartButton != null, "restartButton can't be null");
        }

        public void SetVisible(bool visible)
        {
            gameObject.SetActive(visible);
        }

        public void Init(GameManager gameManager)
        {
            _gameManager = gameManager;
            restartButton.onClick.AddListener(gameManager.RestartGame);
            SetVisible(false);
        }

        public void SetScoreText(int score)
        {
            scoreText.text = $"Your Score: {score}";
        }
    }
}