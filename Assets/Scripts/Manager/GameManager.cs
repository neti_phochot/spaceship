﻿using System;
using SpaceShip;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Random = UnityEngine.Random;

namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public int Score { get; private set; }
        public SpaceShip.EnemyShip EnemyShip { get; private set; }
        public SpaceShip.PlayerShip PlayerShip { get; private set; }

        [SerializeField] private SoundManager soundManager;
        [SerializeField] private EffectManager explodeEffectManager;
        [SerializeField] private HealthBarManager healthBarManager;

        [Header("UI")] [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private EndManager endManager;
        [SerializeField] private DialogManager dialogManager;
        [Header("Prefabs")] [SerializeField] private SpaceShip.PlayerShip playerShipPrefab;
        [SerializeField] private SpaceShip.EnemyShip enemyShipPrefab;

        [Header("Spawn Point")] [SerializeField]
        private float enemySpawnRange = 8;

        [SerializeField] private Transform playerShipPoint;
        [SerializeField] private Transform enemyShipPoint;

        private void Awake()
        {
            Debug.Assert(soundManager != null, "audioManager can't be null");
            Debug.Assert(explodeEffectManager != null, "explodeEffectManager can't be null");
            Debug.Assert(explodeEffectManager != null, "explodeEffectManager can't be null");
            Debug.Assert(scoreManager != null, "scoreManager can't be null");
            Debug.Assert(endManager != null, "endGameManager can't be null");
            Debug.Assert(dialogManager != null, "dialog can't be null");
            Debug.Assert(playerShipPrefab != null, "playerShipPrefab can't be null");
            Debug.Assert(enemyShipPrefab != null, "enemyShipPrefab can't be null");
            Debug.Assert(playerShipPoint != null, "playerShipPoint can't be null");
            Debug.Assert(enemyShipPoint != null, "enemyShipPoint can't be null");

            dialogManager.Init(this);
            endManager.Init(this);

            scoreManager.SetHighScore(SaveManager.GetScore());
            scoreManager.SetScore(0);
        }

        public void StartGame()
        {
            SpawnPlayerShip();
            SpawnEnemyShip();

            soundManager.Init(this);

            endManager.SetVisible(false);
            dialogManager.SetVisible(false);

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        public void RestartGame()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void SpawnPlayerShip()
        {
            PlayerShip = Instantiate(playerShipPrefab, playerShipPoint.position, Quaternion.identity);
            PlayerShip.OnExploded += OnPlayerShipExploded;
            PlayerShip.OnHealthChanged += OnPlayerHealthChanged;
            PlayerShip.RestoreHealth();
        }

        private void SpawnEnemyShip()
        {
            EnemyShip = Instantiate(enemyShipPrefab, enemyShipPoint.position,
                Quaternion.AngleAxis(180, Vector3.forward));
            EnemyShip.OnExploded += OnEnemyShipExploded;
            EnemyShip.RestoreHealth();
        }


        private void OnPlayerShipExploded(BaseSpaceShip ship)
        {
            explodeEffectManager.Play(ship.transform.position);

            PlayerShip.gameObject.SetActive(false);
            EnemyShip.gameObject.SetActive(false);

            endManager.SetScoreText(Score);
            endManager.SetVisible(true);

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            //Save Player High Score
            if (Score > SaveManager.GetScore())
            {
                SaveManager.SetScore(Score);
            }
        }

        private void OnPlayerHealthChanged(float previousHealth, float currentHealth)
        {
            healthBarManager.SetHealth(currentHealth, PlayerShip.MaxHealth);
        }

        private void OnEnemyShipExploded(BaseSpaceShip ship)
        {
            explodeEffectManager.Play(ship.transform.position);

            RespawnEnemyShip();

            scoreManager.SetScore(++Score);
        }

        private void RespawnEnemyShip()
        {
            var newPosition = enemyShipPoint.position;
            newPosition.x = Random.Range(-enemySpawnRange, enemySpawnRange);
            EnemyShip.transform.position = newPosition;
            EnemyShip.RestoreHealth();
        }
    }
}