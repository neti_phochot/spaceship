﻿using UnityEngine;

namespace Manager
{
    public class EffectManager : MonoBehaviour
    {
        [SerializeField] private GameObject effect;

        private void Awake()
        {
            effect.SetActive(false);
        }

        public void Play(Vector3 position)
        {
            effect.transform.position = position;
            effect.SetActive(false);
            effect.SetActive(true);
        }
    }
}