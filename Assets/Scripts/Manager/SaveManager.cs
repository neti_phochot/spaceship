﻿using UnityEngine;

namespace Manager
{
    public static class SaveManager
    {
        private const string ScoreKey = "score";

        public static void SetScore(int score)
        {
            PlayerPrefs.SetInt(ScoreKey, score);
        }

        public static int GetScore()
        {
            return PlayerPrefs.HasKey(ScoreKey) ? PlayerPrefs.GetInt(ScoreKey) : 0;
        }
    }
}