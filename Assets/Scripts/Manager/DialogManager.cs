﻿using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class DialogManager : MonoBehaviour, IVisible
    {
        [SerializeField] private Button startButton;

        private GameManager _gameManager;

        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton can't be null");
        }

        public void SetVisible(bool visible)
        {
            gameObject.SetActive(visible);
        }

        public void Init(GameManager gameManager)
        {
            startButton.onClick.AddListener(gameManager.StartGame);
        }
    }
}