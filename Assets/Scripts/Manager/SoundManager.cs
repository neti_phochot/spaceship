﻿using SpaceShip;
using UnityEngine;
using Utils;

namespace Manager
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        [Range(0, 1f)] [SerializeField] private float spatialBlend;

        [Header("SFX Settings")] [SerializeField]
        private AudioClip[] gunFire;

        [SerializeField] private AudioClip[] shipExplode;
        [SerializeField] private AudioClip loss;
        [SerializeField] private AudioClip[] hitVoice;
        [SerializeField] private AudioClip[] deathVoice;
        [SerializeField] private AudioClip hitShip;

        public AudioClip GunFire => gunFire[UnityEngine.Random.Range(0, gunFire.Length)];
        public AudioClip ShipExplode => shipExplode[UnityEngine.Random.Range(0, shipExplode.Length)];
        public AudioClip HitVoice => hitVoice[UnityEngine.Random.Range(0, hitVoice.Length)];
        public AudioClip DeathVoice => deathVoice[UnityEngine.Random.Range(0, deathVoice.Length)];
        private AudioSource _hitVoiceSource;

        public void Init(GameManager gameManager)
        {
            gameManager.PlayerShip.OnExploded += OnPlayerShipExploded;
            gameManager.PlayerShip.OnGunFire += OnPlayerShipGunFire;
            gameManager.PlayerShip.OnHealthChanged += OnHealthChanged;

            gameManager.EnemyShip.OnExploded += OnEnemyShipExploded;
            gameManager.EnemyShip.OnGunFire += OnEnemyShipGunFire;
        }

        private void OnHealthChanged(float previousHealth, float currentHealth)
        {
            LeanAudio.play(hitShip);

            if (currentHealth <= 0) return;
            if (_hitVoiceSource)
            {
                _hitVoiceSource.Stop();
            }

            _hitVoiceSource = LeanAudio.play(HitVoice);
        }

        //------------------------------[ PLAYER SHIP ]--------------------------------
        private void OnPlayerShipExploded(BaseSpaceShip ship)
        {
            LeanAudio.playClipAt(ShipExplode, ship.transform.position).spatialBlend = spatialBlend;
            LeanAudio.play(loss);
            LeanAudio.play(DeathVoice);
        }

        private void OnPlayerShipGunFire(BaseSpaceShip ship)
        {
            LeanAudio.playClipAt(GunFire, ship.transform.position).spatialBlend = spatialBlend;
        }

        //------------------------------[ ENEMY SHIP ]--------------------------------
        private void OnEnemyShipExploded(BaseSpaceShip ship)
        {
            LeanAudio.playClipAt(ShipExplode, ship.transform.position).spatialBlend = spatialBlend;
        }

        private void OnEnemyShipGunFire(BaseSpaceShip ship)
        {
            LeanAudio.playClipAt(GunFire, ship.transform.position).spatialBlend = spatialBlend;
        }
    }
}