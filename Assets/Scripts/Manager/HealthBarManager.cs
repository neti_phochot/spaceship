﻿using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class HealthBarManager : MonoBehaviour
    {
        [SerializeField] private Image healthBarImage;

        [Header("Health Color")] [SerializeField]
        private Color highHealth;

        [SerializeField] private Color midHealth;
        [SerializeField] private Color lowHealth;

        private void Awake()
        {
            Debug.Assert(healthBarImage != null, "healthBarImage can't be null!");
        }

        public void SetHealth(float health, float maxHealth)
        {
            var healthPercent = Mathf.Clamp01(health / maxHealth);
            if (healthPercent > .4f)
            {
                healthBarImage.color = highHealth;
            }
            else if (healthPercent > .2f)
            {
                healthBarImage.color = midHealth;
            }
            else
            {
                healthBarImage.color = lowHealth;
            }

            healthBarImage.fillAmount = healthPercent;
        }
    }
}