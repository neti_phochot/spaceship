﻿using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private Text highScoreText;
        [SerializeField] private Text scoreText;


        private void Awake()
        {
            Debug.Assert(highScoreText != null, "highScoreText can't be null");
            Debug.Assert(scoreText != null, "scoreText can't be null");
        }

        public void SetHighScore(int score)
        {
            highScoreText.text = $"HIGH SCORE: {score}";
        }

        public void SetScore(int score)
        {
            scoreText.text = $"SCORE: {score}";
        }
    }
}