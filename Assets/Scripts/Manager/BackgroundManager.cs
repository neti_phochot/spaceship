﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Manager
{
    [RequireComponent(typeof(MeshRenderer))]
    public class BackgroundManager : MonoBehaviour
    {
        [SerializeField] private Material[] backgroundMaterials;

        private void Awake()
        {
            GetComponent<MeshRenderer>().material = backgroundMaterials[Random.Range(0, backgroundMaterials.Length)];
        }
    }
}