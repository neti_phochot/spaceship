﻿using System;
using UnityEngine;

namespace Utils

{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool _isShuttinDown = false;
        private static object _lock = new object();
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_isShuttinDown)
                {
                    Debug.LogWarning("Singleton already shutting down!");
                    return null;
                }

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = (T) FindObjectOfType(typeof(T));
                        if (_instance == null)
                        {
                            var singletonObj = new GameObject($"{typeof(T)} (Singleton)");
                            _instance = singletonObj.AddComponent<T>();

                            DontDestroyOnLoad(singletonObj);
                        }
                    }

                    return _instance;
                }
            }
        }

        private void OnApplicationQuit()
        {
            _isShuttinDown = true;
        }

        private void OnDestroy()
        {
            _isShuttinDown = true;
        }
    }
}