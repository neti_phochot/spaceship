﻿using System;
using SpaceShip;

public interface IDamagable
{
    event Action<BaseSpaceShip> OnExploded;
    void TakeDamage(float damage);
    void Explode();
}