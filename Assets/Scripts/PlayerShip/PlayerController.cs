﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;

        [Header("Boundary Settings")] [SerializeField]
        private float padding;

        public float xBound;
        public float yBound;
        private Vector2 movementInput = Vector2.zero;

        private void Awake()
        {
            SetupBoundary();
        }

        private void Update()
        {
            Move();
        }

        public event Action OnGunFire;

        private void SetupBoundary()
        {
            var cam = Camera.main;
            xBound = cam.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            yBound = cam.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
        }

        private void Move()
        {
            var gravity = new Vector2(0, -2);
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed + gravity;
            Debug.DrawRay(transform.position, gravity, Color.green);
            Debug.DrawRay(transform.position, inputDirection, Color.yellow);
            Debug.DrawRay(transform.position, inPutVelocity, Color.red);

            var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;
            newXPos = Mathf.Clamp(newXPos, -xBound, xBound);
            newYPos = Mathf.Clamp(newYPos, -yBound, yBound);

            transform.position = new Vector2(newXPos, newYPos);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            if (context.phase != InputActionPhase.Performed) return;
            OnGunFire?.Invoke();
        }
    }
}