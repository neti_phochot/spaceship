﻿using PlayerShip;
using UnityEngine;

namespace SpaceShip
{
    public class PlayerShip : BaseSpaceShip
    {
        [SerializeField] private PlayerController playerController;

        private void Start()
        {
            Debug.Assert(playerController != null, "playerController can't be null");

            playerController.OnGunFire += Fire;
        }
    }
}