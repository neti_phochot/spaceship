﻿using UnityEngine;

namespace SpaceShip
{
    public class EnemyShip : BaseSpaceShip
    {
        [SerializeField] private float fireDelayMin = 1f;
        [SerializeField] private float fireDelayMax = 1f;

        private float _fireThreshold;

        private void Update()
        {
            EnemyAI();
        }


        private void EnemyAI()
        {
            if (_fireThreshold > 0)
            {
                _fireThreshold -= Time.deltaTime;
                return;
            }

            _fireThreshold = Random.Range(fireDelayMin, fireDelayMax);
            Fire();
        }
    }
}