﻿using System;
using UnityEngine;

namespace SpaceShip
{
    public abstract class BaseSpaceShip : MonoBehaviour, IDamagable
    {
        public event Action<float, float> OnHealthChanged;
        public event Action<BaseSpaceShip> OnExploded;
        public event Action<BaseSpaceShip> OnGunFire;
        public float Health { get; private set; }
        public float MaxHealth => maxHealth;

        [SerializeField] private Transform bulletSpawnPoint;
        [SerializeField] protected float maxHealth = 3f;

        private void Awake()
        {
            Debug.Assert(bulletSpawnPoint != null, "bulletSpawnPoint can't be null");
            Health = maxHealth;
        }

        public void RestoreHealth()
        {
            var previousHealth = Health;
            Health = maxHealth;
            OnHealthChanged?.Invoke(previousHealth, Health);
        }

        public void TakeDamage(float damage)
        {
            var previousHealth = Health;
            Health -= damage;
            OnHealthChanged?.Invoke(previousHealth, Health);

            if (Health > 0) return;

            Explode();
            OnExploded?.Invoke(this);
        }

        public virtual void Explode()
        {
            //TODO: Boom
        }

        public virtual void Fire()
        {
            OnGunFire?.Invoke(this);
            var bullet = BulletPool.GetInstance.Request();
            bullet.transform.position = bulletSpawnPoint.position;
            bullet.Init(transform.up, this);
        }
    }
}