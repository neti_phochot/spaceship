﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ScrollBackground : MonoBehaviour
{
    [SerializeField] private float speed;
    private MeshRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        var offset = new Vector2(0, speed * Time.time);
        _spriteRenderer.material.mainTextureOffset = offset;
    }
}